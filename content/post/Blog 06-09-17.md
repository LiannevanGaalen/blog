---
title: Blog 06-09-17 Week 2
subtitle: Door Lianne van Gaalen
date: 2017-09-06
---

Deze dag stond weer helemaal in het teken van de design challenge. Tot ongeveer 13:00 uur hebben we nog gewerkt aan ons onderzoek. Daarna zijn we ons onderzoek samen met onze challenge leider/mentor gaan bespreken of we op de goede weg waren.

De feedback die ik heb gehad.

- Moodboard lifestyle meer uitbreiden
- Wat zijn nou echte bekende hotspots voor studenten
- Denk aan winkels > drankje doen > lunchen > studiespots
- Waar gaan CMD studenten graag heen?
- Moodboard doelgroep minder stock plaatjes

Voor de rest vond hij dat we goed op weg waren en vooral zo door moesten gaan.

s'Middags hebben wij onze studie coach ( Gerhard Revelt ) leren kennen. Zo heeft ons verschillende dingen uitgelegd.

- Waarvoor kunnen we bij hem terecht?
- Waar help hij allemaal mee?
- Hoe overleg je goed met je team?
- Hoe communiceer je goed met je team?
- Wie hebben je allemaal nodig in je team voor een vergadering?
- Voorzitten > Notulist > Time keeper > Cheerleader
- Oefenen vergadering

Dit was erg handig. Zo heb ik geoefend voor de positie notulist. Ik kan zelf goed verhalen schrijven en uitwerken. Mijn minpunt is helaas dat ik niet altijd luister naar wat die persoon te vertellen heeft, daarom was deze oefening erg handig!

Als laatste hebben wij vandaag geleerd hoe wij onze blogs moeten gaan bijhouden. Zo hebben we geleerd over HTML en wat we het beste kunnen gebruiken als het aankomt op blogs schrijven. Volgende week krijgen wij daar meer uitleg over.
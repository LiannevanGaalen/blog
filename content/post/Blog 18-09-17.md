---
title: Blog 18-09-17 Week 4
subtitle: Door Lianne van Gaalen
date: 2017-09-18
---

KILL YOUR DARLINGS.  Ja het is gebeurt ons eerste project is weg. Toch vind ik het helemaal niet zo erg, want nu ga je weer verder werken aan nieuwe ideeën voor en te gek spel.

We begonnen vandaag met het presenteren en bespreken van ons eerste spel. Één iemand van elk groepje moest het spel, in ons geval wie is de mol, pitchen. En we gingen in groepen het spel met elkaar bespreken. Dit leverde weer interessante feedback op. Toen we het spel met een ander groepje aan het bespreken waren was alles best wel duidelijk, maar toen we het voor heel de klas moesten uitleggen vonden sommige leerlingen het nog steeds te ingewikkeld. 

FEEDBACK PRESENTATIE:

- Wat is het doel van de mol > duidelijker
- De mol niet de mol noemen > Nieuwe naam
- Wat wint de mol > duidelijker
- Hoe weet je tegen wie je speelt > duidelijker
- Gaan meer teams tegen elkaar spelen dan twee teams > Uitzoeken
- Gaan de mensen dan tegen de mol in > vriendelijker maken
- Foto’s/ challenges zijn goed en leuk!
- Hoe regelen we de enveloppen > bewaking van asociale toeristen. 

Na deze presentaties kregen we onze nieuwe briefing. Hier kregen we te horen dat we opnieuw moeten beginnen met ons spel, a.k.a Kill your darlings. Hiervoor hebben we een scrum (planning via een whiteboard met hierop verschillende kopjes en post its) gemaakt. Dit is een techniek die ik zelf nog nooit gebruikt heb.

Na het maken van deze planning en de taakverdeling kregen we nog een workshop over de blog en was het weer tijd om naar huis te gaan.



